use std::time::SystemTime;

/// Very simple helper class which measures how long a function runs.
///
/// Create a timer object e.g. ``let _timer = Timer::new("abc");`` at
/// the beginning of a block, then forget about it.
///
/// When the timer is dropped, it will print the elapsed amount of time
/// as a ``debug!`` message.
///
/// *Note*: If the elapsed time is less than 10ms, it will not be printed.
pub struct Timer {
	start: SystemTime,
	name: String
}

impl Timer {
	/// Create a new timer with the given name.
	pub fn new(name: &str) -> Timer {
		Timer {
			name: String::from(name),
			start: SystemTime::now()
		}
	}
}

impl Drop for Timer {
	fn drop(&mut self) {
		if self.start.elapsed().unwrap().subsec_nanos() > 10000 {
			debug!("Time spent in {}: {:?}", self.name, self.start.elapsed().unwrap());
		}
	}
}
