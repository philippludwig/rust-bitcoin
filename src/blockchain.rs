extern crate mongodb;

use std::{
	collections::{BTreeMap,BTreeSet},
	ops::Index,
};

use bson::{Bson, spec::BinarySubtype};
use self::mongodb::{cursor::Cursor, coll::options::FindOptions};
use timer::Timer;

use database::{Database,DatabaseError,BlockResult};
use protocol::Network;
use message::{
	transaction::{Transaction, TransactionInput, TransactionOutput, TransactionVersion},
	block::{Block, Hash}
};

impl ::std::convert::From<TransactionVersion> for Bson {
	fn from(version: TransactionVersion) -> Bson {
		Bson::from(match version {
			TransactionVersion::V1 => 1i32,
			TransactionVersion::V2 => 2i32
		})
	}
}

impl<'a> ::std::convert::From<&'a bson::Bson> for TransactionVersion {
	fn from(b: &'a Bson) -> TransactionVersion {
		match b.as_i32().unwrap() {
			1 => TransactionVersion::V1,
			2 => TransactionVersion::V2,
			x => panic!("Invalid transaction version {}", x)
		}
	}
}

/// Result information of a block insert operation
#[derive(Debug,PartialEq)]
pub enum InsertError {
	/// Block is already known
	AlreadyKnown,

	/// Block could not be inserted into the DB
	DBFailed,

	/// Tip is broken - somehow invalidated
	InvalidTip
}

#[derive(Clone,Debug,Serialize)]
pub enum BlockChainError {
	BrokenDocument,
	CouldNotFindBlock,
	DatabaseError(DatabaseError),
	EmptyChain,
	InvalidBlock,
	UnknownBlock,
	TipError (super::tip::TipError),
}

impl ::std::convert::From<bson::ValueAccessError> for BlockChainError {
	fn from(err: bson::ValueAccessError) -> BlockChainError {
		error!("Broken BlockChain: {:?}", err);
		error!("{:?}", backtrace::Backtrace::new());
		match err {
			bson::ValueAccessError::NotPresent => BlockChainError::UnknownBlock,
			bson::ValueAccessError::UnexpectedType => BlockChainError::BrokenDocument
		}
	}
}

impl ::std::convert::From<DatabaseError> for BlockChainError {
	fn from(err: DatabaseError) -> BlockChainError {
		BlockChainError::DatabaseError(err)
	}
}

pub struct BlockChain {
	db: Database,
	known_blocks: BTreeSet<Hash>,
	tip: super::Tip,
	network: Network
}

impl BlockChain {
	pub fn new(network: Network, mongo_hostname: &str, port: u16) -> Result<BlockChain,BlockChainError> {
		info!("Connecting to DB");

		let db = Database::new(&network, mongo_hostname, port);

		Ok(BlockChain {
			tip: try!(BlockChain::load_tip(&db.collection, &network)),
			known_blocks: BlockChain::get_known_blocks(&db.collection),
			network,
			db
		})
	}

	// Public DB interface
	pub fn get_newest_orphan(&self) -> BlockResult { self.db.get_newest_orphan() }
	pub fn newest_block(&self) -> BlockResult { self.db.newest_block() }
	pub fn get_oldest_orphan(&self) -> BlockResult { self.db.get_oldest_orphan() }
	pub fn oldest_block(&self) -> BlockResult { self.db.oldest_block() }

	fn load_tip(coll: &mongodb::coll::Collection, network: &Network) -> Result<super::tip::Tip,BlockChainError> {
		// Collect initial Tip Blocks
		let mut initial_tip_blocks = vec!();
		for block_result in try!(BlockChain::initial_tip(&coll)) {
			initial_tip_blocks.push(try!(block_result))
		}

		// If we don't have any blocks, at least use the genesis block.
		if initial_tip_blocks.is_empty() {
			initial_tip_blocks.push(Block::genesis(network));
		}

		super::Tip::new(initial_tip_blocks).map_err(BlockChainError::TipError)
	}

	fn get_known_blocks(collection: &mongodb::coll::Collection) -> BTreeSet<Hash> {
		let mut known_blocks = BTreeSet::new();

		// Retrieve all block hashes
		// Even with 1 mio blocks, this would only take
		// 32000000 byte = 32000 KByte = 32 MB
		let mut options = FindOptions::default();
		options.projection = Some(doc! { "_id" => 1 }); // Return only the ID
		let cursor = collection.find(None, Some(options)).unwrap();
		cursor.into_iter().map(|i| i.unwrap())
			.map(|doc| {
				let mut hash = [0u8; 32];
				hash.copy_from_slice(doc.get_binary_generic("_id").unwrap());
				hash
			})
			.map(|hash| known_blocks.insert(hash))
			.for_each(|_| ()); // Ignore the result of BTreeSet::insert
		info!("Retrieved hashes for {} blocks", known_blocks.len());
		known_blocks
	}

	pub fn clone_tip(&self) -> Vec<Block> { self.tip.clone_blocks() }

	/// Find the "tip" of the blockchain, i.e. the ``size`` newest blocks that are not orphans.
	/// These information can be used for requesting more blocks from the network.
	fn initial_tip(collection: &mongodb::coll::Collection) -> Result<Vec<BlockResult>,BlockChainError> {
		info!("Reading initial Tip");

		// Sort by timestamp, descending
		let filter = doc! { "number" => { "$ne" => -1 } };
		let mut options = FindOptions::default();
		options.sort = Some(doc! { "number" => -1 });
		options.limit = Some(2000);

		let cursor: Cursor = collection.find(Some(filter), Some(options)).unwrap();
		let mut progress = 0;
		Ok(cursor.into_iter()
		  .filter_map(|i| i.or_else(|e| Err(error!("Could not iterate Tip: {:?}", e))).ok())
		  .map(|doc| {
			  progress += 1;
			  if progress % 100 == 0 {info!("{}...", progress);}
			  Database::build_block_from_doc(&doc)
		  })
		  .collect())
	}

	/// Try to insert a new block into the DataBase
	pub fn insert_block(&mut self, block: Block) -> Result<(),InsertError> {
		let _t = Timer::new("insert_block");

		// Check if we already know this block
		// if self.known_blocks.contains(block.get_hash()) { return Err(InsertError::AlreadyKnown); }

		// Add the block to the DB; make a copy if we need it later.
		let mut block_copy = block.clone();
		let number = self.add_block(block);

		// Add it to the list of the known blocks.
		self.known_blocks.insert(block_copy.get_hash().clone());

		// We can add it to the tip if it is not an orphan
		if number != -1 {
			block_copy.number = number;
			self.tip.add_block(block_copy).map_err(|e| {
				error!("Could not insert block: {:?}", e);
				error!("Reloading tip");
				self.tip = BlockChain::load_tip(&self.db.collection, &self.network).unwrap();
				if self.tip.verify().is_err() {
					panic!("Tip is now invalid - why?");
				}
				InsertError::InvalidTip
			})
		} else {
			Ok(())
		}
	}

	fn serialize_txns(txns: Vec<Transaction>) -> Bson {
		Bson::Array({
			txns.into_iter()
			.map(|tx| doc! {
				"version" => tx.version.clone(),
				"lock_time" => i64::from(tx.lock_time),
				"hash" => (BinarySubtype::Generic, tx.hash.clone()),
				"inputs" => BlockChain::serialize_tx_inputs(tx.tx_in),
				"outputs" => BlockChain::serialize_tx_outputs(tx.tx_out)
			})
			.map(Bson::from)
			.collect()
		})
	}

	fn serialize_tx_inputs(inputs: Vec<TransactionInput>) -> Bson {
		Bson::Array({
			inputs.into_iter()
			.map(|input| doc! {
				"previous_output" => doc! {
					"hash" => (BinarySubtype::Generic, input.previous_output.hash),
					"index" => i64::from(input.previous_output.index)
				},
				"script" => (BinarySubtype::Generic, input.script),
				"sequence" => i64::from(input.sequence)
			})
			.map(Bson::from)
			.collect::<Vec<Bson>>()
		})
	}

	fn serialize_tx_outputs(outputs: Vec<TransactionOutput>) -> Bson {
		Bson::Array({
			outputs.into_iter()
			.map(|output| doc! {
				"value" => output.value,
				"pk_script" => (BinarySubtype::Generic, output.pk_script),
				"address" => output.address.clone()
			})
			.map(Bson::from)
			.collect::<Vec<Bson>>()
		})
	}

	/// Return true if we know this block
	pub fn has_block(&self, block: &Block) -> bool {
		let hash = block.get_hash();
		let rev_hash = hash.to_vec().iter().rev().cloned().collect::<Vec<u8>>();
		self.known_blocks.contains(hash) || self.known_blocks.contains(rev_hash.as_slice())
	}

	pub fn find_block(&self, hash: Hash) -> BlockResult {
		self.db.block_by_hash(hash)
	}

	fn genesis_hash_testnet() -> Vec<u8> {
		     //00  00  00  00    09    33    ea    01    ad    0e    e9    84  209779baaec3ced90fa3f408719526f8d77f4943
		vec!(00, 00, 00, 00, 0x09, 0x33, 0xea, 0x01, 0xad, 0x0e, 0xe9, 0x84,
			   0x20, 0x97, 0x79, 0xba, 0xae, 0xc3, 0xce, 0xd9, 0x0f, 0xa3, 0xf4,
			   0x08, 0x71, 0x95, 0x26, 0xf8, 0xd7, 0x7f, 0x49, 0x43).into_iter().collect()
	}

	pub fn find_orphans(&self) -> Vec<BlockResult> {
		let _t = Timer::new("find_orphans");
		let filter = doc! { "number" => -1 };

		let cursor: Cursor = self.db.collection.find(Some(filter), None).unwrap();
		cursor.into_iter()
		  .map(|i| i.unwrap())
		  .map(|doc| Database::build_block_from_doc(&doc))
		  .collect()
	}

	/// Find blocks which have multiple children
	/// Note: Currently this takes very long, locking the BlockChain in the process.
	/// TODO: Can we access the collection parallel from different threads?
	/// (It's a DataBase after all)
	#[allow(dead_code)]
	pub fn find_fork(&self) {
		let _t = Timer::new("find_fork");
		let mut parents = BTreeMap::new();

		// Find two blocks which have the same parent
		self.db.collection.find(None, None).unwrap().into_iter()
			.map(|i| i.unwrap())
			.for_each(|b| {
				let mut prev_block = [0u8; 32];
				prev_block.copy_from_slice(b.get_binary_generic("prev_block").unwrap());

				if parents.contains_key(&prev_block) {
					*parents.get_mut(&prev_block).unwrap() += 1;
					warn!("{} has more than 1 child!", stringutils::byte_array_to_string(&prev_block));
				} else {
					parents.insert(prev_block, 1);
				}
			});
	}

	/// Add a new block to the database
	///
	/// If this block is the parent of an orphan, their status will be changed.
	///
	/// Returns the number of this block or -1 if it is an orphan.
	fn add_block(&mut self, block: Block) -> i64 {
		let _t = Timer::new("add_block");
		// Check if we know the parent of this block.

		let filter = doc! {
			"_id" => (BinarySubtype::Generic, block.prev_block.to_vec())
		};

		let number = {
			if block.prev_block.to_vec() == BlockChain::genesis_hash_testnet() {
				1i64 // The parent is the genesis block and therefore well-known
				        // (This is needed since the network doesn't offer us the genesis block
					// according to stackexchange)
			} else {
				let mut cursor = self.db.collection.find(Some(filter), None).unwrap();

				// If the parent is an orphan, this block is also an orphan
				match cursor.has_next().unwrap() {
					false => -1i64,   // We don't know the parent, so this block is an orphan
					true => match cursor.next().unwrap().unwrap().get_i64("number").unwrap() {
						-1 => -1i64, // The parent is an orphan, so this block is an orphan
						x => x + 1 // Parent is not an orphan, use index + 1
					}
				}
			}
		};

		match number {
			-1 => info!("Orphan: {}", stringutils::byte_array_to_string(block.get_hash())),
			x => info!("Block #{}: {}", x, stringutils::byte_array_to_string(block.get_hash()))
		}

		// Serialize the block & insert it
		let hash = block.get_hash().to_vec();
		let doc = doc! {
			"_id" => (BinarySubtype::Generic, hash.clone()),
			"prev_block" => (BinarySubtype::Generic, block.prev_block.to_vec()),
			"timestamp" => block.timestamp as i64, /* Bson does not support unsigned int */
			"version" => block.version,
			"bits" => block.bits as i64,
			"nonce" => block.nonce as i64,
			"merkle_root" => (BinarySubtype::Generic, block.merkle_root.to_vec().clone()),
			"txns" => BlockChain::serialize_txns(block.txns),
			"number" => number
		};

		self.db.collection.insert_one(doc, None).unwrap();

		// Check if this block is the parent of a block which has been an orphan
		if number != -1 { // Only if this block is not an orphan
			let mut current_hash = hash;
			let mut num = number;

			loop {
				// Find the block which is the child of this block
				let parent = doc! {
					"prev_block" => (BinarySubtype::Generic, current_hash.clone()),
					"number" => -1
				};

				// Set it's number to this block's number + 1
				num += 1;
				let update = doc! { "$set" => { "number" => num } };

				match self.db.collection.find_one_and_update(parent, update, None).unwrap() {
					None => break,  // We could not find a child block
					Some(block) => {
						// Check if this child has other children
						current_hash.copy_from_slice(block.get_binary_generic("_id").unwrap());
					}
				}
			}
		};

		number
	}

	pub fn verify_tip(&self) -> Result<(),BlockChainError> {
		let _t = Timer::new("verify_tip");

		self.tip.verify().map_err(|e| BlockChainError::TipError(e))?;

		Ok(info!("Verified Tip"))
	}

	pub fn verify_blockchain(&self) -> Result<(),BlockChainError> {
		let _t = Timer::new("verify_blockchain");
		// Try verify tip first. If it works, don't check the whole chain.
		let result = self.verify_tip();
		if result.is_ok() {
			return Ok(());
		}

		// Error - Check every block and remove bad blocks
		error!("Errors during Tip verification - checking complete chain");
		let mut bad_blocks: Vec<Hash> = Vec::new();
		let mut options = FindOptions::default();
		options.sort = Some(doc! { "number" => -1 });

		// Find bad blocks
		let mut parent: Option<Block> = None;
		let cursor: Cursor = self.db.collection.find(None, Some(options)).unwrap();
		let mut n = 0;
		let mut smallest_num = ::std::i64::MAX;
		for r in cursor {
			let doc = r.unwrap();
			let block = Database::build_block_from_doc(&doc).unwrap();
			if parent.is_some() {
				let p = parent.as_ref().unwrap();
				let parent_hash = *p.get_hash();
				if block.prev_block != parent_hash {
					if block.number < smallest_num {smallest_num = block.number};
					if p.number < smallest_num {smallest_num = p.number};

					bad_blocks.push(*block.get_hash());
					bad_blocks.push(parent_hash);
				}
			}
			parent = Some(block);
			n += 1;
			if n % 1000 == 0 {
				info!("{}...", n);
			}
		}

		// Remove bad blocks
		info!("Removing {} bad blocks", bad_blocks.len());
		for hash in bad_blocks {
			self.db.collection.delete_one(doc! {
				"$eq" => {"_id" => (BinarySubtype::Generic, hash.to_vec())}
			}, None).unwrap();
		}

		// Mark orphans as orphan
		smallest_num -= 1;
		info!("New tip stops at {}, marking everything after as orphan", smallest_num);
		self.db.collection.update_many(
			doc! { "$gt" => { "number" => smallest_num } },
			doc! { "$set" => { "number" => -1i64 }},
			None
		).unwrap();

		info!("Done.");
		result
	}

	/// Retrieve a collection of hashes for describing the state of the BlockChain,
	/// also known as the "BlockLocator".
	/// ``max_opt`` may be set to the maximum number of hashes; default: *2000*
	pub fn get_blocklocator_object(&self, max_opt: Option<i32>) -> Vec<Block> {
		let _t = Timer::new("get_blocklocator_object");
		let max = max_opt.unwrap_or(2000);
		let mut block_locator = Vec::new();

		let mut tip = Vec::new();
		if self.tip.len() < 2 { // No blocks yet
			info!("No blocks in Tip - empty block locator.");
			return block_locator;
		}
		tip.push(self.tip.clone_block_by_index(0).unwrap());
		tip.push(self.tip.clone_block_by_index(1).unwrap());
		block_locator.append(&mut tip);
		let mut current_height = block_locator.index(1).number;
		let mut step_size = 2;
		while (max as usize > block_locator.len()) && (current_height - step_size > 0) {
			current_height -= step_size;
			step_size *= 2;

			match self.db.block_by_number(current_height) {
				Ok(block) => block_locator.push(block),
				Err(e) => {
					error!("Could not get block with height {}: {:?}", current_height, e);
					break;
				}
			}
		}

		block_locator
	}
}
