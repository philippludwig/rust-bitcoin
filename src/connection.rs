use std::{
	io, io::{ErrorKind,Read,Write},
	net::{IpAddr, TcpStream, SocketAddr},
	time::Duration,
	thread, sync::{Arc, Mutex}
};

use protocol::Network;
use protocol;

#[derive(Clone,PartialEq)]
pub enum State {
	Idle,
	Connecting,
	Connected,
	Disconnected
}

type StatePtr = Arc<Mutex<State>>;
type DataPtr  = Arc<Mutex<Vec<u8>>>;
type QueuePtr = Arc<Mutex<Vec<Vec<u8>>>>;

fn os_windows() -> bool {
	::std::env::consts::EXE_SUFFIX == ".exe"
}

/// Represents a generic p2p connection
#[derive(Clone)]
pub struct Connection {
	/// Current state
	state: StatePtr,

	/// Buffered data
	data: DataPtr,

	/// Queue for data to be written
	queue: QueuePtr,

	/// Remote address
	addr: IpAddr,

	/// Local address
	my_ip: IpAddr
}

impl Connection {
	/// Create a new connection by using the given remote ip and own IP
	pub fn new(addr: IpAddr, my_ip: &IpAddr) -> Connection {
		Connection {
			state: Arc::new(Mutex::new(State::Idle)),
			data: Arc::new(Mutex::new(Vec::new())),
			addr,
			my_ip: *my_ip,
			queue: Arc::new(Mutex::new(Vec::new())),
		}
	}

	/// Take an existing open connection and create a Connection instance
	pub fn from_stream(mut stream: TcpStream, network: &Network) -> io::Result<Connection> {
		// Get IP
		let my_ip = try!(stream.local_addr()).ip();
		let remote_ip = try!(stream.peer_addr()).ip();

		trace!("Connected to {}", remote_ip);
		stream.set_read_timeout(Some(Duration::from_millis(500))).unwrap();

		// Send a version message immediately after connecting
		// TODO: Move this to the BitCoin protocol implementation; see #6
		let version_msg = protocol::create_version_message(&network, &my_ip, &remote_ip).unwrap();
		stream.write(&version_msg).unwrap();

		// Create connection instance
		let connection = Connection::new(remote_ip, &my_ip);

		// Listen to stream
		Connection::handle_data(connection.state.clone(),
			connection.data.clone(), connection.queue.clone(), stream);

		Ok(connection)
	}

	pub fn disconnect(&mut self) {
		*self.state.lock().unwrap() = State::Disconnected
	}

	fn read_all(stream: &mut TcpStream) -> Result<Vec<u8>,io::ErrorKind> {
		let mut result = Vec::new();

		loop {
			let mut buf: [u8; 4096] = [0u8; 4096];
			match stream.read(&mut buf) {
				Ok(val) => if val > 0 {
					result.append(&mut Vec::from(&buf[0..val]));
				} else {
					// Stop reading if we don't have anything left to
					// read at the moment.
					return Ok(result);
				},
				Err(e) => match e.kind() {
					io::ErrorKind::WouldBlock => return Ok(result),
					io::ErrorKind::TimedOut => if os_windows() {
						return Ok(result) } else {
						return Err(io::ErrorKind::TimedOut)
					},
					kind => return Err(kind)
				}
			};
		}
	}

	fn handle_data(state: StatePtr, data: DataPtr, queue: QueuePtr, mut stream: TcpStream) {
		// Read from stream as long as we have a connection
		while *state.lock().unwrap() == State::Connected {
			let no_data;
			match Connection::read_all(&mut stream) {
				Ok(mut new_data) => {
					no_data = new_data.is_empty();
					if !no_data {
						trace!("Read {} bytes", new_data.len());
						data.lock().unwrap().append(&mut new_data);
						trace!("Added data to queue");
					}
				},
				Err(e) => {
					error!("Error reading from stream: {:?}", e);
					break;
				}
			};

			// Send everything in the queue
			let queue_empty = queue.lock().unwrap().is_empty();
			{
				let mut q_lock = queue.lock().unwrap();
				{let q: &Vec<Vec<u8>> = &*q_lock;
				for data in q.iter() {
					trace!("Sending {} bytes", &data.len());
					if let Err(e) = stream.write(&data) {
						error!("Could not send data to client: {:?}", e);
						error!("Closing connection, ignoring remaining data to send in queue.");
						*state.lock().unwrap() = State::Disconnected;
						break;
					}
					trace!("Sent.");
				}}
				(*q_lock).clear();
			}

			if no_data && queue_empty { // Nothing to do
				thread::sleep(::std::time::Duration::from_millis(1));
			}
		}
		*state.lock().unwrap() = State::Disconnected;
	}

	pub fn start(&self, network: Network) {
		let addr = self.addr;
		let data = self.data.clone();
		let state = self.state.clone();
		let queue = self.queue.clone();
		let my_ip = self.my_ip;
		thread::spawn(move || {
			trace!("Connecting to: {}", addr);
			*state.lock().unwrap() = State::Connecting;
			let mut stream = match TcpStream::connect(SocketAddr::from((addr, match network {
				Network::MainNet => 8333,
				Network::TestNet3 => 18333
			}))) {
				Ok(s) => s,
				Err(e) => {
					*state.lock().unwrap() = State::Disconnected;
					return error!("Could not connect to {}: {:?}", addr, e);
				}
			};
			*state.lock().unwrap() = State::Connected;
			trace!("Connected to {}", addr);
			stream.set_read_timeout(Some(Duration::from_millis(500))).unwrap();

			// Send a version message immediately after connecting
			// TODO: Move this to the BitCoin protocol implementation; see #6
			let version_msg = protocol::create_version_message(&network, &my_ip, &addr).unwrap();

			stream.write(&version_msg).unwrap();
			trace!("{}: Send VERSION message", addr);

			Connection::handle_data(state, data, queue, stream);

			info!("Disconnected from {}", addr);
		});
	}

	pub fn get_state(&self) -> State {
		self.state.lock().unwrap().clone()
	}

	pub fn get_remote_ip(&self) -> IpAddr {
		self.addr
	}

	/// Get the number of bytes currently in the queue
	pub fn get_queue_size(&self) -> usize {
		self.queue.lock().unwrap()
			.iter()
			.fold(0 as usize, |acc, ref v| acc+v.len())
	}

	/// Return true if data is available.
	pub fn has_data(&self) -> bool {
		!self.data.lock().unwrap().is_empty()
	}

	/// Read all currently available data.
	pub fn read_data(&self) -> Vec<u8> {
		let data = self.data.lock().unwrap().clone();
		self.data.lock().unwrap().clear();
		data
	}

	pub fn write(&self, data: &[u8]) -> Result<(),ErrorKind> {
		match self.queue.lock() {
			Ok(mut q) => Ok(q.push(Vec::from(data))),
			Err(e) => {
				error!("Could not write to queue, failed to acquire lock:\n{:?}", e);
				Err(ErrorKind::Other)
			}
		}
	}
}
