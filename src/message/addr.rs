use std::io::Cursor;
use std::net::{IpAddr,Ipv4Addr,Ipv6Addr};

use bytes::*;

use message::header::Header;
use message::Message;
use protocol::*;

/// Take a bytestream which represents a bitcoin IpAddress and
/// convert it to a Rust IpAddr.
fn decode_address(bytestream: &[u8]) -> IpAddr {
	// Check if the first 12 bytes are of this format:
	// 00 00 00 00 00 00 00 00 00 00 FF FF
	// If this is the case, we are dealing with an IPv4 address
	// according to: https://en.bitcoin.it/wiki/Protocol_documentation#Network_address
	let mut first_10_zeroes = true;
	for x in bytestream.iter().take(10) {
		if *x != 0u8 {
			first_10_zeroes = false;
			break;
		}
	}

	if first_10_zeroes && (bytestream[10] == 0xFF) && (bytestream[11] == 0xFF) {
		// IpV4
		IpAddr::from(
			Ipv4Addr::new(bytestream[12], bytestream[13],
				bytestream[14], bytestream[15])
		)
	} else {
		// IPv6
		let mut ipv6_bytes: Vec<u16> = Vec::new();
		let mut cursor = Cursor::new(bytestream);
		for _ in 0..8 {
			ipv6_bytes.push(cursor.get_u16_le());
		}
		IpAddr::from(
			Ipv6Addr::new(ipv6_bytes[0], ipv6_bytes[1], ipv6_bytes[2],
				ipv6_bytes[3], ipv6_bytes[4], ipv6_bytes[5],
				ipv6_bytes[6], ipv6_bytes[7],
			)
		)
	}
}

pub fn from_bytestream(header: &Header) -> Result<Message,ProtocolError> {
	let mut cursor = Cursor::new(&header.payload);

	// Read VarInt containing number of addresses
	let num_addresses = try!(read_varint(&mut cursor));
	trace!("Message contains {} addresses", num_addresses);

	// TODO: Check if protocol version of this peer is below
	// 31402, in this case don't expect timestamp or better
	// ignore the peer list.
	// See note here: https://en.bitcoin.it/wiki/Protocol_documentation#addr
	let mut addresses: Vec<(IpAddr,u16)> = Vec::new();
	for _ in 0..num_addresses {
		let _timestamp = cursor.get_u32_le();
		let _services  = cursor.get_u64_le();

		let mut ip_bytes: Vec<u8> = Vec::new();
		ip_bytes.resize(16, 0u8);
		cursor.copy_to_slice(ip_bytes.as_mut_slice());

		let ip = decode_address(&ip_bytes);

		let port = cursor.get_u16_be();

		trace!("Got Address: {}:{}", ip, port);
		addresses.push( (ip,port) );
	}

	Ok(Message::Addr {
		addresses
	})
}
