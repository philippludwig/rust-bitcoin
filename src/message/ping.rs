use std::io::Cursor;

use bytes::*;

use message::{header::Header, Message};

pub fn from_bytestream(header: &Header) -> Message {
	let mut cursor = Cursor::new(&header.payload);

	// Get Nonce
	let nonce = cursor.get_u64_le();

	Message::Ping {
		nonce
	}
}
