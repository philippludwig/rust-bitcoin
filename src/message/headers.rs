use std::io::Cursor;

use message::{block::Block, header::Header,Message};
use protocol;

pub fn from_bytestream(header: &Header, network: &protocol::Network) -> Result<Message,protocol::ProtocolError> {
	let mut cursor = Cursor::new(&header.payload);

	let num_headers = protocol::read_varint(&mut cursor)?;
	let mut block_headers = Vec::new();
	for _ in 0..num_headers {
		let mut header = Block::from_cursor(&mut cursor, network)?;
		block_headers.push(header);
	}

	Ok(Message::Headers {
		block_headers
	})
}
