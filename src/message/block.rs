/// Everything for dealing with bitcoin blocks as they are transmitted over the network.

use std::io::Cursor;

use bytes::*;

use message::{
	header,
	header::Header,
	transaction::Transaction
};
use protocol::*;

pub type Hash = [u8; 32];

/// Represents a BitCoin block as it is transmitted over the network.
///
/// This struct follows the definition found in the developer reference:
///
/// https://bitcoin.org/en/developer-reference#block-headers
#[derive(Clone,Debug,Serialize)]
pub struct Block {
	/// Block version as defined in the protocol.
	pub version: i32,

	/// Hash of the previous block.
	pub prev_block: Hash,

	/// Merkle root hash (see developer reference for details).
	pub merkle_root: Hash,

	/// Number of this block.
	///
	/// This is not part of the official specification, but used
	/// internal by this library to easily find a block by it's number.
	/// The genesis block is #0, the first "real" block is #1, etc.
	pub number: i64,

	/// Unix timestamp in seconds when this block was created.
	pub timestamp: u32,

	/// Encoded target threshhold; see developer reference for details.
	pub bits: u32,

	/// An arbitrary number used together with the target threshhold.
	pub nonce: u32,

	/// Hash of this block.
	pub hash: Hash,

	/// All transactions stored in this block.
	pub txns: Vec<Transaction>
}

impl Block {
	/// Read a byte-encoded block from the given cursor.
	pub fn from_cursor(mut cursor: &mut Cursor<&Vec<u8>>, network: &Network) -> Result<Block,ProtocolError> {
		// Just read everything from the stream.
		let version = cursor.get_i32_le();

		let mut prev_block: Hash = [0u8; 32];
		cursor.copy_to_slice(&mut prev_block);
		prev_block.reverse();

		let mut merkle_root: Hash = [0u8; 32];
		cursor.copy_to_slice(&mut merkle_root);

		let timestamp = cursor.get_u32_le();
		let bits = cursor.get_u32_le();
		let nonce = cursor.get_u32_le();

		let num_tx = try!(read_varint(&mut cursor));

		trace!("Building block");
		let mut block = Block {
			version,
			prev_block,
			merkle_root,
			timestamp,
			bits,
			nonce,
			hash: [0u8; 32],
			txns: Block::read_transactions(&mut cursor, num_tx, network)?,
			number: -2    // Not known at this point; will be assigned by BlockChain
		};
		trace!("Built block.");
		block.hash = block.calc_hash();
		Ok(block)
	}

	/// Extract a block from a generic bitcoin message header.
	pub fn from_bytestream(header: &Header, network: &Network) -> Result<Block,ProtocolError> {
		trace!("Parsing Block from bytestream");
		let mut cursor = Cursor::new(&header.payload);

		Block::from_cursor(&mut cursor, network)
	}

	/// Create the genesis block.
	///
	/// This block is never transmitted over the blockchain,
	/// therefore we have to create it ourself from scratch.
	pub fn genesis(network: &Network) -> Block {
		Block {
			version: 1,
			prev_block: [0u8; 32],
			merkle_root: [0u8; 32],
			timestamp: 0,
			bits: 0,
			nonce: 0,
			hash: match network {
				Network::TestNet3 => [
					00, 00, 00, 00, 0x09, 0x33, 0xea, 0x01, 0xad, 0x0e, 0xe9, 0x84,
			   		0x20, 0x97, 0x79, 0xba, 0xae, 0xc3, 0xce, 0xd9, 0x0f, 0xa3, 0xf4,
			   		0x08, 0x71, 0x95, 0x26, 0xf8, 0xd7, 0x7f, 0x49, 0x43
				],
				Network::MainNet => panic!("Feature not implemented!")
			},
			txns: vec!(),
			number: 0
		}
	}

	/// Calculate the hash of this block as defined by the protocol.
	fn calc_hash(&self) -> Hash {
		let mut hash_bytes = Vec::new();
		hash_bytes.put_i32_le(self.version);
		hash_bytes.put_slice(&self.prev_block.iter().cloned().rev().collect::<Vec<u8>>());
		hash_bytes.put_slice(&self.merkle_root);
		hash_bytes.put_u32_le(self.timestamp);
		hash_bytes.put_u32_le(self.bits);
		hash_bytes.put_u32_le(self.nonce);

		// Reverse hash, since this is the default for bitcoin
		let vec_hash: Vec<u8> = header::hash_payload(&hash_bytes).into_iter().rev().collect();
		let mut hash: [u8; 32] = [0u8; 32];
		hash.copy_from_slice(&vec_hash[0..32]);
		hash
	}

	/// Return a reference to the hash.
	pub fn get_hash(&self) -> &Hash {
		&self.hash
	}

	/// Return a reverse copy of the hash of this block
	pub fn get_rev_hash(&self) -> Hash {
		let rev_hash: Vec<u8> = self.hash.iter().rev().cloned().collect();
		let mut result: [u8; 32] = [0u8; 32];
		result.copy_from_slice(&rev_hash[0..32]);
		result
	}

	/// Read ``num_tx`` number of transactions from the given bytestream.
	fn read_transactions(mut cursor: &mut Cursor<&Vec<u8>>, num_tx: u64, network: &Network) -> Result<Vec<Transaction>,ProtocolError> {
		let mut txns = Vec::new();

		for _ in 0..num_tx {
			txns.push(try!(Transaction::from_cursor(&mut cursor, network)));
		}
		assert_eq!(txns.len(), num_tx as usize);

		Ok(txns)
	}
}

