mod addr;
pub mod block;
mod header;
mod headers;
pub mod inv;
mod ping;
pub mod transaction;
mod version;

use std::net::IpAddr;

use self::header::Header;
pub use self::block::Block;
use self::transaction::Transaction;
use protocol::{Network, ProtocolError};

#[derive(Debug)]
pub enum ProtocolVersion {
	Unknown,
	P31402, P60002, P70012, P70013, P70014, P70015, P80002, P80003
}

/// Services as defined in the BitCoin protocol that are supported by a node
#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum Service {
	/// This node may be asked for full blocks
	Network,

	GetUtxo, Bloom, Witness, NetworkLimited
}

#[derive(Debug)]
#[allow(dead_code)]
pub enum Message {
	/// Represents a BitCoin VERSION message
	Version {
		protocol_version: ProtocolVersion,
		timestamp: i64,
		services: Vec<Service>
	},
	/// Represents a BitCoin VERACK message.
	/// Besides the header, these are empty.
	Verack,
	/// Represents BitCoin TX message
	Transaction (Transaction),
	Block (Block),
	/// Represents a BitCoin GETADDR message.
	/// This message is used to request addresses of additional
	/// peers to connect to and doesn't carry any data.
	GetAddr,
	/// Reply message of GetAddr
	Addr { addresses: Vec<(IpAddr,u16)> },
	/// Ping, Pong
	Ping { nonce: u64}, Pong {nonce: u64},
	/// Inventory Message
	Inv {
		inventory_entries: Vec<self::inv::InventoryVector>
	},
	Headers {
		/// Important: These blocks do not have transactions, since
		/// headers message do not contain transactions!
		block_headers: Vec<block::Block>
	}
}

pub use self::header::ParseError;

#[derive(Debug)]
pub enum InvalidMessage {
	ParseError(ParseError),
	ProtocolError(ProtocolError)
}

pub struct Messages {
	bytes: Vec<u8>,
	network: Network
}

impl Messages {
	pub fn new(data: Vec<u8>, network: Network) -> Messages {
		Messages {
			bytes: data,
			network
		}
	}

	fn next_header(&mut self) -> Result<header::Header,InvalidMessage> {
		Header::from_bytes(&mut self.bytes)
			.map(|parse_result| {
				self.bytes = parse_result.additional_data;
				parse_result.header
			})
			.map_err(|e| InvalidMessage::ParseError(e))
	}

	fn build_message(h: Header, network: &Network) -> Option<Result<Message,InvalidMessage>> {
		match h.command.as_str() {
			"alert" => None,
			"headers" => Some(headers::from_bytestream(&h, network).map_err(|e| InvalidMessage::ProtocolError(e))),
			"getheaders" => None, /* TODO */
			"version" => Some(Ok(version::from_bytestream(&h))),
			"verack" => Some(Ok(Message::Verack)),
			"getaddr" => Some(Ok(Message::GetAddr)),
			"addr" => Some(addr::from_bytestream(&h).map_err(|e| InvalidMessage::ProtocolError(e))),
			"ping" => Some(Ok(ping::from_bytestream(&h))),
			"inv" => Some(inv::from_bytestream(&h).map_err(|e| InvalidMessage::ProtocolError(e))),
			"notfound" => None, /* Ignore for now */
			"block" => {
				trace!("Forwarding block");
				Some(Ok(Message::Block(match Block::from_bytestream(&h, network) {
					Ok(b) => b,
					Err(e) => return Some(Err(InvalidMessage::ProtocolError(e)))
				}
				)))
			},
			"tx" => Some(Ok(Message::Transaction(
				match Transaction::from_bytestream(&h, network) {
					Ok(v) => v,
					Err(e) => return Some(Err(InvalidMessage::ProtocolError(e)))
				}
			))),
			_ => Some(
				Err(
					InvalidMessage::ParseError (
						ParseError::UnknownCommand {
							command: h.command
						}
					)
				)
			)
		}
	}
}

impl Iterator for Messages {
	type Item = Result<Message,InvalidMessage>;

	fn next(&mut self) -> Option<Self::Item> {
		if self.bytes.is_empty() { return None; }

		let header = match self.next_header() {
			Ok(h) => h,
			Err(e) => return Some(Err(e))
		};

		Messages::build_message(header, &self.network)
	}
}
